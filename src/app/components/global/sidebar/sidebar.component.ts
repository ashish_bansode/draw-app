import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  dragStartHandler(event){
    event.dataTransfer.setData("text/plain", event.target.id);
    event.dataTransfer.dropEffect = "copy";
  }

  dropHandler(event){
    event.preventDefault();
    var id = event.dataTransfer.getData("text/plain")
    var element = document.getElementById(id)
    var elementType = element.id.split('_')[0]
    element.id = elementType;
    if(elementType === "rectangle"){
      element.style.height = '20px';
      element.style.width = '35px';
    }
    element.innerHTML = element.id;
    event.target.appendChild(element)
  }

  dragOverHandler(event){
    event.preventDefault();
    event.dataTransfer.dropEffect = "copy";
  }

}
