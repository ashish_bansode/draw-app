import { Component, OnInit, ViewChild } from '@angular/core';

import { mxgraph } from 'mxgraph';

declare var require: any;

const mx: typeof mxgraph = require('mxgraph')({
	mxBasePath: 'mxgraph'
});

@Component({
  selector: 'app-draw-area',
  templateUrl: './draw-area.component.html',
  styleUrls: ['./draw-area.component.css']
})
export class DrawAreaComponent implements OnInit {

  showElementDetails: boolean = false;
  elements: any = {}
  selectedElementDetails: any = {}
  static scope: DrawAreaComponent;
  /*
  The elements object will have the following structure:
  {
    rectangle: {
      properties: {
        rectangle_1 : {
          height: '10px',
          width: '20px'
        },
        rectangle_2 : {
          height: '5px',
          width: '10px'
        }
      },
      last_id: 2
    },
    square: {
      properties: {
        square_1 : {
          width: '10px'
        },
        square_2: {
          width: '10px'
        },
        square_3: {
          width: '10px'
        }
      },
      last_id: 3
    }
  }

  */

  constructor() {
    DrawAreaComponent.scope = this
   }

  ngOnInit() {
  }

  dragStartHandler(event){
    event.dataTransfer.setData("text/plain", event.target.id);
    event.dataTransfer.dropEffect = "copy";
  }

  dropHandler(event){
    event.preventDefault();
    var id = event.dataTransfer.getData("text/plain")
    var element = document.getElementById(id)
    if(id === "rectangle"){
      //scaling rectangle figure
      element.style.height = '100px';
      element.style.width = '200px';
    }
    this.registerElement(element);
    element.addEventListener('click', this.showElementProperties)
    event.target.appendChild(element)
  }

  dragOverHandler(event){
    event.preventDefault();
    event.dataTransfer.dropEffect = "copy";
  }

  registerElement(element){
    var elementType = element.id; // E.g. rectangle, square, circle, etc.

    if(!(elementType in this.elements)){
      this.elements[elementType] = {
        properties: {},
        last_id: 0
      }
    }
    var newID = element.id + '_' + (++this.elements[element.id].last_id); // E.g. rectangle_10
    this.elements[elementType].properties[newID] = this.getInitialElementProperties(element);
    element.id = newID;
    element.innerHTML = element.id;
  }

  getInitialElementProperties(element){
    switch(element.id){
      case 'rectangle' : return {
        height: element.style.height,
        width: element.style.width
      }
      case 'square' : return {
        height: element.style.height
      }
    }
  }

  showElementProperties(event){
    var elementType = event.target.id.split('_')[0];
    DrawAreaComponent.scope.selectedElementDetails = DrawAreaComponent.scope.elements[elementType].properties[event.target.id];
  }

  @ViewChild('graphContainer', {static:false}) graphContainer;

  ngAfterViewInit() {
    const graph = new mx.mxGraph(this.graphContainer.nativeElement);

    try {
      const parent = graph.getDefaultParent();
      graph.getModel().beginUpdate();

      
      const vertex1 = graph.insertVertex(parent, '1', 'Vertex 1', 10, 100, 50, 20);
      // const vertex2 = graph.insertVertex(parent, '2', 'Vertex 2', 0, 0, 50, 20);

      // graph.insertEdge(parent, '', '', vertex1, vertex2);
      // console.log("mxgraph.mxConstants.STYLE_MOVABLE: " + mxgraph.mxConstants.STYLE_MOVABLE)
      // graph.setCellStyles(mxgraph.mxConstants.STYLE_MOVABLE, '0', [vertex1])
      graph.setCellStyles('movable', '0', [vertex1])


      // vertex1.model.addListener('click',function(){
      //   console.log("Vertex 1 clicked")
      // })

      var graphModel = graph.getModel();
      // console.log("graphmodel " + graphModel.isVertex())
      graph.model.addListener('change', function(){
        console.log("change detected")
      })

      graph.addMouseListener(
        {
          cell: null,
          isMouseDown: false,
          mouseDown: function(sender, me) {
            this.cell = me.getCell();
            this.isMouseDown = true;
            //  console.log("mouseDown"); console.log(me.sourceState.cell.value) 
            },
          mouseMove: function(sender, me)
          {
            // console.log("mouseMove")
            // console.log(me)
            var tmp = me.getCell();
        
            if ( (tmp != this.cell) && !this.isMouseDown )
            {
              if (this.cell != null)
              {
                this.dragLeave(me.getEvent(), this.cell);
              }
        
              this.cell = tmp;
        
              if (this.cell != null)
              {
                this.dragEnter(me.getEvent(), this.cell);
              }
            }
        
            if (this.cell != null)
            {
              this.dragOver(me.getEvent(), this.cell);
            }
          },
          mouseUp: function(sender, me) { 
            // var tmp = me.getCell();
            if (this.cell != null)
            {
              this.drop(me.getEvent(), this.cell);
            }
            this.isMouseDown = false;
            console.log("mouseUp"); console.log(me)
           },
          dragEnter: function(evt, cell)
          {
            console.log("dragEnter")
            // mxLog.debug('dragEnter', cell.value);
          },
          dragOver: function(evt, cell)
          {
            console.log("dragOver")
            // mxLog.debug('dragOver', cell.value);
          },
          dragLeave: function(evt, cell)
          {
            console.log("dragLeave")
            // mxLog.debug('dragLeave', cell.value);
          },
          drop: function(evt,cell){
            console.log("drop")
            console.log(evt)
            console.log(cell)
            var pt = graph.getPointForEvent(evt, true)
            console.log(pt)
            // var newCell = graph.cloneCells([cell],true);
            // graph.setCellStyles('movable', '1', newCell)
            // var newCell = new mxCell()
            if(!graph.isCellMovable(cell)){
              var cells = graph.importCells([cell], pt.x, pt.y, graph.getDefaultParent(), evt, null);
              graph.setCellStyles('movable', '1', cells)
              // graph.resizeCells(cells, new mxRectangleShape(100,80), true)
              // cells[0].geometry.width = '100';
              // cells[0].geometry.height = '80';   
              // graph.setCellStyles('width', '100', cells) 
              // cells[0].style.width = 100;
              // cells[0].style.height = 80;
            }
          }
        });
      
      // var encoder = new mxCodec();
      // var encodedNode = encoder.encode(graph.getModel())
      // mxUtils.getXml(encodedNode)
      
      // var abc = new mxEventObject();
      // console.log( parent.getXml())
        this.initGraphConnectionHandler(graph)
    } finally {
      graph.getModel().endUpdate();
      new mx.mxHierarchicalLayout(graph).execute(graph.getDefaultParent());
    }
  }

  initGraphConnectionHandler(graph){
    graph.setConnectable(true);
    // mxgraph.mxConnectionHandler.prototype.connectImage = new mxImage('images/green-dot.gif', 14, 14);
    graph.connectionHandler.addListener('connect',function(sender,evt){
      var edge = evt.getProperty('cell');
      var source = graph.getModel().getTerminal(edge, true);
      var target = graph.getModel().getTerminal(edge, false);
    
      var style = graph.getCellStyle(edge);
      // var sourcePortId = style[mxConstants.STYLE_SOURCE_PORT];
      // var targetPortId = style[mxConstants.STYLE_TARGET_PORT];
    
      // mxLog.show();
      // mxLog.debug('connect', edge, source.id, target.id, sourcePortId, targetPortId);
    })
  }

}
