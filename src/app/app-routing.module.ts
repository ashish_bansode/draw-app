import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DrawAreaComponent } from "./components/draw-area/draw-area.component"


const routes: Routes = [
  { path: 'draw-area', component: DrawAreaComponent},
  { path: '**', redirectTo: 'draw-area'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
